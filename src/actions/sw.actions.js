import axios from 'axios';

export const SW_LIST_ADD = 'LIST_ADD';
export const SW_LIST_ADD_NEW = 'SW_LIST_ADD_NEW';
export const SW_LIST_SHOW_DETAIL = 'SW_LIST_SHOW_DETAIL';
export const SW_LIST_DELETE_ITEM = 'SW_LIST_DELETE_ITEM';
export const SW_LIST_SEARCH_ITEM = 'SW_LIST_SEARCH_ITEM';
export const SW_DETAIL_ADD = 'DETAIL_ADD';

export const addSwAction = (payload) => {
    return {
        type: SW_LIST_ADD,
        payload
    }
}

export const addNewSwAction = (payload) => {
    return {
        type: SW_LIST_ADD_NEW,
        payload
    }
}

export const showSwListDetailAction = (payload) => {
    return {
        type: SW_LIST_SHOW_DETAIL,
        payload
    }
}

export const searchSwItemAction = (payload) => {
    return {
        type: SW_LIST_SEARCH_ITEM,
        payload
    }
}

export const addSwDetailAction = (payload) => {
    return {
        type: SW_DETAIL_ADD,
        payload
    }
}

export const deleteSwItemAction = (payload) => {
    return {
        type: SW_LIST_DELETE_ITEM,
        payload
    }
}



export const loadSwData = (slug) => {
    return dispatch => {
        axios.get(`https://swapi.dev/api/${slug}/`)
            .then(res => res.data.results)
            .then(data => {
                dispatch(addSwAction(data));
            })
            .catch(err => {
                alert(err.message);
            });
    }
}

export const loadSwDetailData = (slug, id) => {
    return dispatch => {
        axios.get(`https://swapi.dev/api/${slug}/${id}/`)
            .then(res => res.data)
            .then(data => {
                dispatch(addSwDetailAction(data));
            })
            .catch(err => {
                alert(err.message);
            });
    }
}