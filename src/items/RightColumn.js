import React from "react";

export const RightColumn = () => {
    return (
        <div className="right">
            <div className="themes">
                <h1>Main Themes</h1>
                <div className="theme_navigation">
                    <a href="/people">people</a>
                    <a href="/planets">planets</a>
                    <a href="/films">films</a>
                    <a href="/species">species</a>
                    <a href="/vehicles">vehicles</a>
                    <a href="/starships">starships</a>
                    <a href="/404">Not found</a>
                </div>
            </div>
        </div>
    )
}