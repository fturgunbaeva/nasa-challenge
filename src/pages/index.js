import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Switch,
    Redirect,
} from "react-router-dom";

import {Home} from './Home'
import {Login} from "./Login";
import {NotFound} from "./NotFound";
import {SwList} from "./SwList";
import {fakeAuth} from "../data/users"


function PrivateRoute({children, ...rest}) {
    return (
        <Route
            {...rest}
            render={({location}) =>
                fakeAuth.isAuthenticated ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/login",
                            state: {from: location}
                        }}
                    />
                )
            }
        />
    );
}

export const AppRouter = () => {

    return (
        <Router>
            <Switch>

                <Route path="/login">
                    <Login/>
                </Route>
                <Route path="/404">
                    <NotFound/>
                </Route>
                <PrivateRoute path="/:slug">
                    <SwList/>
                </PrivateRoute>
                <Route path="/">
                    <Home/>
                </Route>
            </Switch>
        </Router>
    )
}